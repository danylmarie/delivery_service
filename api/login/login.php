<?php 
	//Headers
	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json');

	include_once '../../config/Database.php';
	include_once '../../models/User.php';

	//Instatiate  DB and Connect
	$database = new Database();
	$db = $database->connect();

	// Instantiate user object
    $user = new User($db);
    
    $user->username = isset($_GET['username']) ? $_GET['username'] : die();
    $user->password =  isset($_GET['password']) ? $_GET['password'] : die();

    $result = $user->login();

    if($result->rowCount() > 0){
        $row = $result->fetch(PDO::FETCH_ASSOC);

        $user_arr=array(
            "status" => true,
            "message" => "Succesfully Login!",
            "id" => $row['id'],
            "username" => $row['username']
        );
    }
    else{
        $user_arr=array(
            "status" => false,
            "message" => "Invalid Username or Password",
        );
    }

    print_r(json_encode($user_arr));