<?php 
	//Headers
	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json');

	include_once '../../config/Database.php';
	include_once '../../models/User.php';

	//Instatiate  DB and Connect
	$database = new Database();
	$db = $database->connect();

	// Instantiate user object
	$user = new User($db);

	//Get user Query
	$result = $user->allusers();

	//Get row count
	$num = $result->rowCount();

	//Check if there is a user
	if($num > 0){

		$user_arr = array();
		$user_arr['data'] = array();

		while($row = $result->fetch(PDO::FETCH_ASSOC)){
			extract($row);

			$user_item = array(
				'name' => $name,
				'username' => $username,
				'address' => $address,
				'usertype' => $usertype
			);

			//Push to "data"
			array_push($user_arr['data'], $user_item);
		}

		// Turn to JSON & output
		echo json_encode($user_arr);

	}else{
		//No User
		echo json_encode(
			array('message' => 'No Users Found')
		);
	}



