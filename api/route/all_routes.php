<?php 
	//Headers
	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json');

	include_once '../../config/Database.php';
	include_once '../../models/Route.php';

	//Instatiate  DB and Connect
	$database = new Database();
	$db = $database->connect();

	// Instantiate routes object
	$route = new Route($db);

	//Route Query
	$result = $route->allroutes();

	//Get row count
	$num = $result->rowCount();

	//Check if there is a route
	if($num > 0){
		//Boo array
		$route_arr = array();
		$route_arr['data'] = array();

		while($row = $result->fetch(PDO::FETCH_ASSOC)){
			extract($row);

			$route_item = array(
				'id' => $id,
				'origin' => $origin,
				'destination' => $destination,
				'route_name' => $route_name,
				'delivery_time'=> $delivery_time,
				'delivery_cost'=> $delivery_cost,
				'status'=> $status
			);

			//Push to "data"
			array_push($route_arr['data'], $route_item);
		}

		// Turn to JSON & output
		echo json_encode($route_arr);

	}else{
		//No posts
		echo json_encode(
			array('message' => 'No Route Found')
		);
	}



