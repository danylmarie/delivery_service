<?php 
	//Headers
	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json');
	header('Access-Control-Allow-Methods: POST');
	header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization, X-Requested-With');

	include_once '../../config/Database.php';
	include_once '../../models/Route.php';

	//Instatiate  DB and Connect
	$database = new Database();
	$db = $database->connect();

	//Instantiate route object
	$route = new Route($db);

	//Get raw posted data
	$data = json_decode(file_get_contents("php://input"));

	$route->origin = $data->origin;
	$route->destination = $data->destination;
	$route->route_name = $data->route_name;
	$route->delivery_time = $data->delivery_time;
	$route->delivery_cost = $data->delivery_cost;
	$route->status = $data->status;

	//Create Route
	if($route->createroute()){
		echo json_encode(
			array('message' => 'Route Created')
		);
	} else{
		echo json_encode(
			array('message' => 'Route Not Created')
		);
	}