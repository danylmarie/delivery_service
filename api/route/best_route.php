<?php 
	//Headers
	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json');

	include_once '../../config/Database.php';
	include_once '../../models/Route.php';

	//Instatiate  DB and Connect
	$database = new Database();
	$db = $database->connect();

	// Instantiate route object
	$route = new Route($db);

	$route->origin = isset($_GET['origin']) ? $_GET['origin'] : die();
	$route->destination = isset($_GET['destination']) ? $_GET['destination'] : die();
	
	//Route Query
	$result = $route->bestroute();

	//Get row count
	$num = $result->rowCount();

	//Check if there is a route
	if($num > 0){
		//Array Route
		$route_arr = array();
		$route_arr['data'] = array();

		while($row = $result->fetch(PDO::FETCH_ASSOC)){
			extract($row);

			$route_item = array(
				'id' => $id,
				'route_name' => $route_name,
				'delivery_time' => $delivery_time
			);

			//Push to "data"
			array_push($route_arr['data'], $route_item);
		}

		// Turn to JSON & output
		echo json_encode($route_arr);

	}else{
		//No posts
		echo json_encode(
			array('message' => 'No Best Route Found')
		);
	}

