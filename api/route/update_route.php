<?php 
	//Headers
	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json');
	header('Access-Control-Allow-Methods: PUT');
	header('Access-Control-Allow-Headers: Access-Control-Allow-Headers, Content-Type, Access-Control-Allow-Methods, Authorization, X-Requested-With');

	include_once '../../config/Database.php';
	include_once '../../models/Route.php';

	//Instatiate  DB and Connect
	$database = new Database();
	$db = $database->connect();

	//Instantiate Route object
	$route = new Route($db);

	//Get raw posted data
	$data = json_decode(file_get_contents("php://input"));

	//Set ID to update
	$route->id = $data->id;

	$route->origin = $data->origin;
	$route->destination = $data->destination;
	$route->route_name = $data->route_name;
	$route->delivery_time = $data->delivery_time;
	$route->delivery_cost = $data->delivery_cost;
	$route->status = $data->status;

	//Upate route
	if($route->updateroute()){
		echo json_encode(
			array('message' => 'Route Updated')
		);
	} else{
		echo json_encode(
			array('message' => 'Route Not Updated')
		);
	}