-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 17, 2020 at 08:56 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.3.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `delivery_service`
--

-- --------------------------------------------------------

--
-- Table structure for table `points`
--

CREATE TABLE `points` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `description` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `points`
--

INSERT INTO `points` (`id`, `name`, `description`) VALUES
(1, 'A', 'Point A'),
(2, 'B', 'Point B'),
(3, 'C', 'Point C'),
(4, 'D', 'Point D'),
(5, 'E', 'Point E'),
(6, 'F', 'Point F'),
(7, 'G', 'Point G'),
(8, 'H', 'Point H'),
(9, 'I', 'Point I');

-- --------------------------------------------------------

--
-- Table structure for table `routes`
--

CREATE TABLE `routes` (
  `id` int(200) NOT NULL,
  `origin` varchar(200) NOT NULL,
  `destination` varchar(200) NOT NULL,
  `route_name` varchar(200) NOT NULL,
  `delivery_time` int(200) NOT NULL,
  `delivery_cost` int(200) NOT NULL,
  `status` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `routes`
--

INSERT INTO `routes` (`id`, `origin`, `destination`, `route_name`, `delivery_time`, `delivery_cost`, `status`) VALUES
(1, 'A', 'B', 'A - C - B', 2, 32, 'Active'),
(2, 'A', 'B', 'A - H- E - D - F - I - B', 148, 112, 'Active'),
(3, 'A', 'B', 'A - H - E - D - F - G - B', 112, 80, 'Active'),
(4, 'A', 'C', 'A - E - C', 4, 25, 'Active'),
(5, 'A', 'C', 'A - E - B - C', 7, 20, 'Active'),
(6, 'A', 'D', 'A - H - E - D', 14, 37, 'Active'),
(7, 'A', 'D', 'A - E - D', 7, 40, 'Active'),
(8, 'A', 'E', 'A - H - E', 40, 2, 'Active'),
(9, 'A', 'F', 'A - E - D - F', 36, 60, 'Active'),
(10, 'A', 'F', 'A - H - E - D - F', 86, 50, 'Active'),
(11, 'A', 'G', 'A - E - D - F - G', 77, 110, 'Active'),
(12, 'A', 'G', 'A - H - E - D - F - G', 78, 116, 'Active'),
(13, 'A', 'I', 'A - H - E -D - F - I', 83, 116, 'Active'),
(14, 'A', 'I', 'A - C - B - I', 67, 37, 'Active'),
(15, 'B', 'C', 'B - I - C ', 54, 32, 'Active'),
(16, 'B', 'D', 'B- C - D ', 54, 32, 'Active'),
(17, 'B', 'D', 'B - I - D', 75, 20, 'Active'),
(18, 'B', 'E', 'B - I - E', 70, 15, 'Active'),
(19, 'B', 'E', 'B - I - F - D - E ', 117, 110, 'Active'),
(20, 'B', 'F', 'B - G - F ', 104, 123, 'Active'),
(22, 'B', 'F', 'B - I - F ', 98, 100, 'Active'),
(23, 'B', 'G', 'B- I - F -G ', 20, 40, 'Active'),
(24, 'B', 'G', 'B - G - F - I', 60, 30, 'Active'),
(25, 'C', 'D', 'C - B - I - F - D', 70, 40, 'Active'),
(26, 'C', 'D', 'C - B - I -D', 46, 70, 'Active'),
(27, 'C', 'E', 'C - B - E', 15, 50, 'Active'),
(28, 'C', 'E', 'C - B - I - E', 50, 30, 'Active'),
(29, 'C', 'F', 'C - B - I - F ', 40, 70, 'Active'),
(30, 'C', 'F', 'C- G - B -F', 35, 80, ''),
(31, 'C', 'G', 'C - B -G ', 37, 60, 'Active'),
(32, 'C', 'G', 'C - B - I - G', 45, 57, 'Active'),
(33, 'C', 'I', 'C - B - I ', 20, 27, 'Active'),
(34, 'C', 'I', 'C - B - G - I ', 43, 37, 'Active'),
(35, 'D', 'E', 'D -F -G -C - A - E ', 130, 98, 'Active'),
(36, 'D', 'E', 'D - F - I - E ', 70, 110, 'Active'),
(37, 'D', 'F', 'D - I - F', 30, 40, 'Active'),
(38, 'D', 'F', 'D -E - I - F', 70, 67, 'Active'),
(39, 'D', 'G', 'D -F - I - B - G ', 156, 120, 'Active'),
(40, 'D', 'G', 'D - F - G ', 60, 112, 'Active'),
(41, 'D', 'I ', 'D - F - I', 23, 69, 'Active'),
(42, 'D', 'I', 'D - F - G - I ', 70, 50, 'Active'),
(43, 'E', 'I', 'E - D - F - I', 50, 90, 'Active'),
(44, 'E', 'I', 'E - D - F - G - B - I', 70, 40, 'Active'),
(45, 'E', 'F', 'E - D - F ', 10, 30, 'Active'),
(46, 'E', 'F', 'E - A - C - B - G - F', 160, 119, 'Active'),
(47, 'F', 'G', 'F - I - B - G', 80, 90, 'Active'),
(48, 'F', 'G', 'F - I - G ', 35, 78, 'Active'),
(49, 'G', 'I', 'G - B - I ', 23, 40, 'Active'),
(50, 'G', 'I ', 'G - B - C - A - E - D - F - I ', 190, 150, 'Active'),
(51, 'B', 'I ', 'B - G - I ', 30, 90, 'Active'),
(52, 'B', 'I', 'B - G - I - D', 70, 67, ''),
(53, 'C', 'H', 'C - A - H ', 15, 20, ''),
(54, 'C', 'H', 'C - E - A - H', 30, 25, ''),
(55, 'D', 'H', 'D - E - H', 7, 26, 'Active'),
(56, 'D ', 'H', 'D - C - E -A - H', 40, 30, ''),
(57, 'E', 'H', 'E - D - H ', 17, 30, 'Active'),
(58, 'E', 'H', 'E - A - H ', 15, 19, ''),
(59, 'F', 'H', 'F - D - H ', 13, 20, 'Active'),
(60, 'F', 'H ', 'F - D - E - H ', 40, 35, ''),
(61, 'G', 'H', 'G - F - D - H ', 118, 90, 'Active'),
(62, 'G', 'H ', 'G - F - I - E - D - H ', 170, 79, 'Active'),
(63, 'H', 'I', 'H - D - I ', 23, 37, 'Active'),
(64, 'H', 'I ', 'H - D - F - I', 40, 37, 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(200) NOT NULL,
  `name` varchar(200) NOT NULL,
  `username` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `address` varchar(200) NOT NULL,
  `usertype` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `password`, `address`, `usertype`) VALUES
(1, 'danyl marie', 'xlaylax', 'xlaylax', 'Mobile Legends', 'customer');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `points`
--
ALTER TABLE `points`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `routes`
--
ALTER TABLE `routes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `points`
--
ALTER TABLE `points`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `routes`
--
ALTER TABLE `routes`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
