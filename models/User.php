<?php

	class User {
		//DB Stuff
		private $conn;
		private $table = 'users';

		//Post Properties
		public $id;
		public $name;
		public $username;
		public $password;
		public $address;
		public $usertype;

		//Constructor with DB
		public function __construct($db){
			$this->conn = $db;
		}

		//Get All User
		public function allusers(){
			//create query
			$query = 'SELECT *
			FROM
				' . $this->table . ' 
			ORDER BY
				id ASC';

			// Prepare statement
			$stmt = $this->conn->prepare($query);

			//Execute
			$stmt->execute();

			return $stmt;
		}

		//Add User
		public function register(){
			//Create query
			$query = 'INSERT INTO ' . 
				$this->table . '
			SET
				name = :name,
				username = :username,
				password = :password,
				address = :address,
				usertype = :usertype';

			//Prepare statement
			$stmt = $this->conn->prepare($query);

			//Clean Data			
			$this->name = htmlspecialchars(strip_tags($this->name));
			$this->username = htmlspecialchars(strip_tags($this->username));
			$this->password = htmlspecialchars(strip_tags($this->password));
			$this->address = htmlspecialchars(strip_tags($this->address));
			$this->usertype = htmlspecialchars(strip_tags($this->usertype));
			
			//Bind Data
			$stmt->bindParam(':name', $this->name);
			$stmt->bindParam(':username', $this->username);
			$stmt->bindParam(':password', $this->password);
			$stmt->bindParam(':address', $this->address);
			$stmt->bindParam(':usertype', $this->usertype);

			//Execute Query
			if($stmt->execute()){
				return true;
			}

			// Print error if something goes wrong
			printf("Error: %s. \n, $stmt->error");
			return false;
		}

		// Login 
		public function login(){

			//create query
			$query = "SELECT 
				`id`, `username`, `password`
			FROM
				" . $this->table . " 
			WHERE
				username='" .$this->username . "'
			AND
				password= '" .$this->password . "'";

			// Prepare statement
			$stmt = $this->conn->prepare($query);

			//Execute
			$stmt->execute();

			return $stmt;
		}

}