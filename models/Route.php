<?php

	class Route {
		//DB Stuff
		private $conn;
		private $table = 'routes';

		//Post Properties
		public $id;
		public $origin;
		public $destination;
		public $route_name;
		public $delivery_time;
		public $delivery_cost;
		public $status;

		//Constructor with DB
		public function __construct($db){
			$this->conn = $db;
		}

		//Get All Routes
		public function allroutes(){
			//create query
			$query = 'SELECT
				*
			FROM
				' . $this->table . '
			ORDER BY
				origin ASC';

		// Prepare statement
		$stmt = $this->conn->prepare($query);

		//Execute
		$stmt->execute();

		return $stmt;
		}

		//Create Route
		public function createroute(){
			//Create query
			$query = 'INSERT INTO ' . 
				$this->table . '
			SET
				origin = :origin,
				destination = :destination,
				route_name = :route_name,
				delivery_time = :delivery_time,
				delivery_cost = :delivery_cost,
				status = :status';

			//Prepare statement
			$stmt = $this->conn->prepare($query);

			//Clean Data
			$this->origin = htmlspecialchars(strip_tags($this->origin));
			$this->destination = htmlspecialchars(strip_tags($this->destination));
			$this->route_name = htmlspecialchars(strip_tags($this->route_name));
			$this->delivery_time = htmlspecialchars(strip_tags($this->delivery_time));
			$this->delivery_cost = htmlspecialchars(strip_tags($this->delivery_cost));
			$this->status = htmlspecialchars(strip_tags($this->status));

			//Bind Data
			$stmt->bindParam(':origin', $this->origin);
			$stmt->bindParam(':destination', $this->destination);
			$stmt->bindParam(':route_name', $this->route_name);
			$stmt->bindParam(':delivery_time', $this->delivery_time);
			$stmt->bindParam(':delivery_cost', $this->delivery_cost);
			$stmt->bindParam(':status', $this->status);

			//Execute Query
			if($stmt->execute()){
				return true;
			}

			// Print error if something goes wrong
			printf("Error: %s. \n, $stmt->error");
			return false;
		}

		//Update Route
		public function updateroute(){
			//Create query
			$query = 'UPDATE ' . 
				$this->table . '
			SET
				origin = :origin,
				destination = :destination,
				route_name = :route_name,
				delivery_time = :delivery_time,
				delivery_cost = :delivery_cost,
				status = :status
			WHERE 
				id = :id';

			//Prepare statement
			$stmt = $this->conn->prepare($query);

			//Clean Data
			$this->origin = htmlspecialchars(strip_tags($this->origin));
			$this->destination = htmlspecialchars(strip_tags($this->destination));
			$this->route_name = htmlspecialchars(strip_tags($this->route_name));
			$this->delivery_time = htmlspecialchars(strip_tags($this->delivery_time));
			$this->delivery_cost = htmlspecialchars(strip_tags($this->delivery_cost));
			$this->status = htmlspecialchars(strip_tags($this->status));
			$this->id = htmlspecialchars(strip_tags($this->id));

			//Bind Data
			$stmt->bindParam(':origin', $this->origin);
			$stmt->bindParam(':destination', $this->destination);
			$stmt->bindParam(':route_name', $this->route_name);
			$stmt->bindParam(':delivery_time', $this->delivery_time);
			$stmt->bindParam(':delivery_cost', $this->delivery_cost);
			$stmt->bindParam(':status', $this->status);
			$stmt->bindParam(':id', $this->id);

			//Execute Query
			if($stmt->execute()){
				return true;
			}

			// Print error if something goes wrong
			printf("Error: %s. \n, $stmt->error");
			return false;
		}

		public function deleteroute(){

			//create query 
			$query = 'DELETE FROM ' . $this->table . ' WHERE id = :id';

			//Prepare statement
			$stmt = $this->conn->prepare($query);

			//Clean data
			$this->id = htmlspecialchars(strip_tags($this->id));

			//Bind ID
			$stmt->bindParam(':id', $this->id);

			//Execute Query
			if($stmt->execute()){
				return true;
			}

			// Print error if something goes wrong
			printf("Error: %s. \n, $stmt->error");
			return false;
		}

		//Return Best Route
		public function bestroute(){

			//create query
			$query = 'SELECT
				id,
				route_name,
				MIN(delivery_time),
				delivery_time
			FROM
				' . $this->table . '
			WHERE
				origin = ?
			AND 
				destination = ?';

			// Prepare statement
			$stmt = $this->conn->prepare($query);

			//Bind Param
			$stmt->bindParam(1, $this->origin);
			$stmt->bindParam(2, $this->destination);
			
			//Execute
			$stmt->execute();

			return $stmt;
		}
	}